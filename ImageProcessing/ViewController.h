//
//  ViewController.h
//  ImageProcessing
//
//  Created by User on 10/29/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, atomic) IBOutlet UIImageView *imgView;
- (IBAction)loadPhoto:(id)sender;

@property (weak, atomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIButton *buttonLoad;

@end

