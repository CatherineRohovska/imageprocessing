//
//  ViewController.m
//  ImageProcessing
//
//  Created by User on 10/29/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
<UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    UIImage* inputImage;
    NSMutableArray* filterArray;
    NSMutableDictionary* filters;
    CIContext *context;
}
@end

@implementation ViewController
- (void) defineFilterArray: (CIImage*) image
{
    context = [CIContext contextWithOptions:nil];
    filterArray = [[NSMutableArray alloc] init];
    filters = [[NSMutableDictionary alloc] init];
    CIFilter *filter = [CIFilter filterWithName:@"CISepiaTone"
                                  keysAndValues: kCIInputImageKey, image,
                        @"inputIntensity", @0.8, nil];
    [filters setObject:filter forKey:@"Sepia"];
    [filterArray addObject:@"Sepia"];
    filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:image forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:5.0f] forKey:@"inputRadius"];
    [filters setObject:filter forKey:@"Blur"];
    [filterArray addObject:@"Blur"];
    
    filter = [CIFilter filterWithName:@"CIColorPosterize"];
    [filter setValue:image forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:5.0f] forKey:@"inputLevels"];
    [filters setObject:filter forKey:@"Posterize"];
    [filterArray addObject:@"Posterize"];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [UIApplication sharedApplication].statusBarHidden = YES;
    NSString *filePath =
    [[NSBundle mainBundle] pathForResource:@"art.jpg" ofType:nil];
    NSURL *fileNameAndPath = [NSURL fileURLWithPath:filePath];
    inputImage = [UIImage imageNamed:@"art.jpg"];
    _imgView.contentMode = UIViewContentModeScaleAspectFit;
  
    CIImage *beginImage = [CIImage imageWithContentsOfURL:fileNameAndPath];
    [self defineFilterArray:beginImage];
  //  _indicator.hidden = YES;
    [_indicator stopAnimating];
    
    // 4
   // UIImage *newImage = [UIImage imageWithCIImage:outputImage];
    _imgView.image = inputImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString* name = [filterArray objectAtIndex:indexPath.item];
    [cell.textLabel setText: name];
    return cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return filterArray.count;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
     NSString* name = [filterArray objectAtIndex:indexPath.item];
    CIFilter *filter = [filters objectForKey:name];
     [_indicator startAnimating];
   // CGImageRef cgimg;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
       // _indicator.hidden = NO;
        _buttonLoad.enabled = NO;
        CIImage *outputImage = [filter outputImage];
        
        CGImageRef cgimg = [context createCGImage:outputImage fromRect:CGRectMake(0, 0, inputImage.size.width, inputImage.size.height)];
        
        UIImage *newImage = [UIImage imageWithCGImage:cgimg];
        
        
        // 4
        CGImageRelease(cgimg);
        // UIImage *newImage = [UIImage imageWithCIImage:outputImage];
        _imgView.contentMode = UIViewContentModeScaleAspectFit;
      //  _indicator.hidden = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imgView.image = newImage;
             [_indicator stopAnimating];
            _buttonLoad.enabled = YES;
        });
        
        
        
    });
    
   
    // 3
  
    
}
- (IBAction)loadPhoto:(id)sender {
    UIImagePickerController *pickerC =
    [[UIImagePickerController alloc] init];
    pickerC.delegate = self;
    [self presentViewController:pickerC animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self dismissViewControllerAnimated:YES completion:nil];
  //  NSLog(@"%@", info);
    UIImage *gotImage =
    [info objectForKey:UIImagePickerControllerOriginalImage];
    inputImage = gotImage;
    CIImage *beginImage = [CIImage imageWithCGImage:gotImage.CGImage];
    [self defineFilterArray:beginImage];
    _imgView.image = inputImage;
   // beginImage =
    
    
   // [self amountSliderValueChanged:self.amountSlider];
}

- (void)imagePickerControllerDidCancel:
(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
